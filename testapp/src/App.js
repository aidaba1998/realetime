import React from 'react';
import {db} from './Firebase-conf';
import {uid} from 'uid';
import { useState,useEffect} from 'react';

import{set,ref, onValue,remove, update} from 'firebase/database';

function App() {
  const [todo,setTodo]=useState("");
  const [todos,setTodos]=useState([]);
  const[isEdit,setIsEdit]=useState(false);
  const[templuuid,setTemplUuid]=useState('');
  const  handChange=(e)=>{
    setTodo(e.target.value);
    
  }
  //write//
  const writeToDatabase=()=>{
    const uuid=uid();
    set(ref(db, `/${uuid}` ),{
      todo,
      uuid,
    });
    setTodo("");
  };
  //read//
  useEffect(() => {
    setTodos([]);
    onValue(ref(db),(snapshot)=>{
      const data=snapshot.val();
      if(data!==null){
        Object.values(data).map((todo)=>{
          setTodos((oldArray)=>[...oldArray,todo]);
        });
      };
    });
    
  }, []);
  //update//
  const handUpdate=(todo)=>{
    setIsEdit(true);
    setTemplUuid(todo.uuid);
    setTodo(todo.todo)
  };
  const handSubmit=()=>{
    update(ref(db,`/${templuuid}`),{
      todo,
      uuid: templuuid,
       
    });
  setTodo('');
  setIsEdit(false) ;
};
  //delete//
const handDelete=(todo)=>{
remove(ref(db,`/${todo.uuid}`));
};
  return (
    <div className='App'>
     <input type="test" value={todo} onChange={ handChange}/>
    {isEdit?(
      <>
      <button  onClick={handSubmit}>submit-change</button>
      <button  onClick={()=>
      {
        setIsEdit(false)
      setTodo('')
      }
      }>x</button>
      </>
    ): (<button  onClick={writeToDatabase}>submit</button>)}
      {todos.map((todo)=>(
        <>
          <h1>{todo.todo}</h1>
          <button onClick={()=>handUpdate(todo)}>update</button>
          <button onClick={()=>handDelete(todo)}>delete</button>
        </>
      ))}
    </div>
  )
}

export default App
//npm install firebase//  
//npm install uid//               