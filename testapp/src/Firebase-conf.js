
import { initializeApp } from "firebase/app";
import {getDatabase} from  'firebase/database';

const firebaseConfig = {
  apiKey: "AIzaSyAD6NPIAL98JFJ0FD6YGnGegpm-GZHA4AM",
  authDomain: "fb-crud-21873.firebaseapp.com",
  projectId: "fb-crud-21873",
  storageBucket: "fb-crud-21873.appspot.com",
  messagingSenderId: "1026908373584",
  appId: "1:1026908373584:web:0d9f6116014176bce28755"
};


const app = initializeApp(firebaseConfig);
export const db =getDatabase(app);